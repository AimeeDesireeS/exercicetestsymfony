<?php

namespace App\DataFixtures;

use App\Entity\Author;
use App\Entity\Genre;
use App\Entity\Book;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;

        $genreFantasy = $this->createGenre('Roman');
        $authorMartin = $this->createAuthor(
            'Victor Hugo',
            \DateTime::createFromFormat('Y-m-d', '1802-02-26'),
            'Besançon',
            \DateTime::createFromFormat('Y-m-d', '1885-05-22'),
            'Paris',
            58,
        );
        $newBook = $this->createBook(
            'L\'etranger',
            \DateTime::createFromFormat('Y-m-d', '1802-02-26'),
            $authorMartin = $this->createAuthor(
                'Albert Camus',
                \DateTime::createFromFormat('Y-m-d', '1802-02-26'),
                'Besançon',
                \DateTime::createFromFormat('Y-m-d', '1885-05-22'),
                'Paris',
                58),
            $genreFantasy = $this->createGenre('Roman')
            );

        $genreFantasy = $this->createGenre('Fantasy');
        $authorMartin = $this->createAuthor(
            'George R. R. Martin',
            \DateTime::createFromFormat('Y-m-d', '1948-09-20'),
            'Bayonne, New Jersey, États-Unis',
            null,
            null,
            54
        );
        

        $manager->flush();
    }

    protected function createAuthor(
        string $name,
        \DateTime $brithDate = null,
        string $brithPlace = null,
        \DateTime $deathDate = null,
        string $deathPlace = null,
        int $age
        
    ) {
        $author = new Author();
        $author->setName($name);
        $author->setBirthDate($brithDate);
        $author->setBirthPlace($brithPlace);
        $author->setDeathDate($deathDate);
        $author->setDeathPlace($deathPlace);
        $author->setAge($age);
        $this->manager->persist($author);

        return $author;
    }

    protected function createGenre(string $name)
    {
        $genre = new Genre();
        $genre->setName($name);

        $this->manager->persist($genre);

        return $genre;
    }
    protected function createBook(string $name, \DateTime $PublicationDate = null, Author $author, Genre $genre)
    {
        $book = new Book();
        $book ->setName($name);
        $book->setPublicationDate($PublicationDate);
        $book->setAuthor($author);
        $book->setGenre($genre);
        $this->manager->persist($book);

        return $book ;
    }
}
